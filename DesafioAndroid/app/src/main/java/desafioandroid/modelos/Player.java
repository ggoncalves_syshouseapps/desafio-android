package desafioandroid.modelos;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Gilson on 16/09/2015.
 */
public class Player implements Parcelable {
    private long id;
    private String name;
    private String location;
    private long followers_count;
    private long draftees_count;
    private long likes_count;
    private long likes_received_count;
    private long comments_count;
    private long comments_received_count;
    private String url;
    private String avatar_url;
    private String username;
    private String twitter_screen_name;
    private String website_url;
    private long drafted_by_player_id;
    private long shots_count;
    private long following_count;
    private String created_at;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(long followers_count) {
        this.followers_count = followers_count;
    }

    public long getDraftees_count() {
        return draftees_count;
    }

    public void setDraftees_count(long draftees_count) {
        this.draftees_count = draftees_count;
    }

    public long getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(long likes_count) {
        this.likes_count = likes_count;
    }

    public long getLikes_received_count() {
        return likes_received_count;
    }

    public void setLikes_received_count(long likes_received_count) {
        this.likes_received_count = likes_received_count;
    }

    public long getComments_count() {
        return comments_count;
    }

    public void setComments_count(long comments_count) {
        this.comments_count = comments_count;
    }

    public long getComments_received_count() {
        return comments_received_count;
    }

    public void setComments_received_count(long comments_received_count) {
        this.comments_received_count = comments_received_count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTwitter_screen_name() {
        return twitter_screen_name;
    }

    public void setTwitter_screen_name(String twitter_screen_name) {
        this.twitter_screen_name = twitter_screen_name;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }

    public long getDrafted_by_player_id() {
        return drafted_by_player_id;
    }

    public void setDrafted_by_player_id(long drafted_by_player_id) {
        this.drafted_by_player_id = drafted_by_player_id;
    }

    public long getShots_count() {
        return shots_count;
    }

    public void setShots_count(long shots_count) {
        this.shots_count = shots_count;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public long getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(long following_count) {
        this.following_count = following_count;
    }

    public Player(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getName());
        dest.writeString(getLocation());
        dest.writeLong(getFollowers_count());
        dest.writeLong(getFollowing_count());
        dest.writeLong(getLikes_count() );
        dest.writeLong(getLikes_received_count());
        dest.writeString(getUrl());
        dest.writeString(getAvatar_url());
        dest.writeString(getUsername());
        dest.writeString(getTwitter_screen_name());
        dest.writeString(getWebsite_url());
        dest.writeLong(getDrafted_by_player_id());
        dest.writeLong(getShots_count());
        dest.writeLong(getFollowing_count());
        dest.writeString(getCreated_at());
    }
    public Player(Parcel parcel){

        setId(parcel.readLong());
        setName(parcel.readString());
        setLocation(parcel.readString());
        setFollowers_count(parcel.readLong());
        setFollowing_count(parcel.readLong());
        setLikes_count(parcel.readLong());
        setLikes_received_count(parcel.readLong());
        setUrl(parcel.readString());
        setAvatar_url(parcel.readString());
        setUsername(parcel.readString());
        setTwitter_screen_name(parcel.readString());
        setWebsite_url(parcel.readString());
        setDrafted_by_player_id(parcel.readLong());
        setShots_count(parcel.readLong());
        setFollowing_count(parcel.readLong());
        setCreated_at(parcel.readString());
    }

    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>(){
        @Override
        public Player createFromParcel(Parcel source) {
            return new Player(source);
        }
        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}
