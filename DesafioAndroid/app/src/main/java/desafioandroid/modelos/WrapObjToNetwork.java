package desafioandroid.modelos;

import org.json.JSONObject;

/**
 * Created by Gilson on 18/09/2015.
 */
public class WrapObjToNetwork extends JSONObject {

    private Shot mShot;
    private boolean isNewer;

    public WrapObjToNetwork(Shot shot, boolean isNewer) {
        this.mShot = shot;
        this.isNewer = isNewer;
    }

    public Shot getmShot() {
        return mShot;
    }

    public void setmShot(Shot mShot) {
        this.mShot = mShot;
    }

    public boolean isNewer() {
        return isNewer;
    }

    public void setIsNewer(boolean isNewer) {
        this.isNewer = isNewer;
    }
}
