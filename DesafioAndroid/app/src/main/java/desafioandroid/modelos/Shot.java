package desafioandroid.modelos;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Gilson on 16/09/2015.
 */
public class Shot implements Parcelable {
    private long id;
    private long id_page;
    private String title;
    private String description;
    private long height;
    private long width;
    private long likes_count;
    private long comments_count;
    private long rebounds_count;
    private String url;
    private String short_url;
    private long views_count;
    private long rebound_source_id;
    private String image_url;
    private String image_teaser_url;
    private String image_400_url;
    private Player player = new Player();
    private String created_at;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public long getId_page() {
        return id_page;
    }

    public void setId_page(long id_page) {
        this.id_page = id_page;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }

    public long getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(long likes_count) {
        this.likes_count = likes_count;
    }

    public long getComments_count() {
        return comments_count;
    }

    public void setComments_count(long comments_count) {
        this.comments_count = comments_count;
    }

    public long getRebounds_count() {
        return rebounds_count;
    }

    public void setRebounds_count(long rebounds_count) {
        this.rebounds_count = rebounds_count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShort_url() {
        return short_url;
    }

    public void setShort_url(String short_url) {
        this.short_url = short_url;
    }

    public long getViews_count() {
        return views_count;
    }

    public void setViews_count(long views_count) {
        this.views_count = views_count;
    }

    public long getRebound_source_id() {
        return rebound_source_id;
    }

    public void setRebound_source_id(long rebound_source_id) {
        this.rebound_source_id = rebound_source_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getImage_teaser_url() {
        return image_teaser_url;
    }

    public void setImage_teaser_url(String image_teaser_url) {
        this.image_teaser_url = image_teaser_url;
    }

    public String getImage_400_url() {
        return image_400_url;
    }

    public void setImage_400_url(String image_400_url) {
        this.image_400_url = image_400_url;
    }


    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Shot(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getImage_teaser_url());
        dest.writeString(getImage_400_url());
        dest.writeString(getUrl());
        dest.writeString(getShort_url());
        dest.writeString(getImage_url() );
        dest.writeLong(getId());
        dest.writeLong(getId_page());
        dest.writeString(getTitle());
        dest.writeString(getDescription());
        dest.writeLong(getHeight());
        dest.writeLong(getWidth());
        dest.writeLong(getLikes_count() );
        dest.writeLong(getComments_count());
        dest.writeLong(getRebound_source_id());
        dest.writeLong(getRebounds_count());
        dest.writeLong(getViews_count());
        dest.writeString(getCreated_at());
        dest.writeParcelable(getPlayer(),flags);
    }
    public Shot(Parcel parcel){

        setImage_teaser_url(parcel.readString());
        setImage_400_url(parcel.readString());
        setUrl(parcel.readString());
        setShort_url(parcel.readString());
        setImage_url(parcel.readString());
        setId(parcel.readLong());
        setId_page(parcel.readLong());
        setTitle(parcel.readString());
        setDescription(parcel.readString());
        setHeight(parcel.readLong());
        setWidth(parcel.readLong());
        setLikes_count(parcel.readLong());
        setComments_count(parcel.readLong());
        setRebound_source_id(parcel.readLong());
        setRebounds_count(parcel.readLong());
        setViews_count(parcel.readLong());
        setCreated_at(parcel.readString());
        setPlayer((Player) parcel.readParcelable(Shot.class.getClassLoader()));
    }
    public static final Parcelable.Creator<Shot> CREATOR = new Parcelable.Creator<Shot>(){
        @Override
        public Shot createFromParcel(Parcel source) {
            return new Shot(source);
        }
        @Override
        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };
}
