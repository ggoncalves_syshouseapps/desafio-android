package ggc.desafioandroid.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Extras.Util;
import desafioandroid.modelos.Shot;
import desafioandroid.modelos.WrapObjToNetwork;
import ggc.desafioandroid.ActivityShot;
import ggc.desafioandroid.adapter.AdapterShotRecycler;
import ggc.desafioandroid.R;
import ggc.desafioandroid.network.NetworkConnection;
import ggc.desafioandroid.network.Transaction;
import interfaces.RecyclerViewOnClickListenerHack;


public class FragmentListaShots extends Fragment implements RecyclerViewOnClickListenerHack, Transaction {

    protected static final String TAG = "LOG";
    protected RecyclerView mRecyclerView;
    protected Activity mActivity;
    protected List<Shot> list_ListaItens;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected ProgressBar mPbLoad;
    protected boolean isLastItem;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        //EventBus.getDefault().register(this);
        mActivity = activity;
    }

    @Override
    public void onStop() {
        super.onStop();
        NetworkConnection.getInstance(getActivity()).getRequestQueue().cancelAll( FragmentListaShots.class.getName() );
    }

    @Override
    public View onCreateView(           LayoutInflater inflater,
                             @Nullable  ViewGroup container,
                             @Nullable  Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_lista_shots, container, false);

        if (list_ListaItens == null){
            list_ListaItens = new ArrayList<>();
        }


        mPbLoad = (ProgressBar) layout.findViewById(R.id.pb_load);
        mRecyclerView = (RecyclerView) layout.findViewById(R.id.rvShots);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                AdapterShotRecycler adapter = (AdapterShotRecycler) mRecyclerView.getAdapter();

                if (!isLastItem && list_ListaItens.size() == llm.findLastCompletelyVisibleItemPosition() + 1 && (mSwipeRefreshLayout == null || !mSwipeRefreshLayout.isRefreshing())) {
                    Toast.makeText(getActivity(), String.valueOf(list_ListaItens.get(llm.findLastCompletelyVisibleItemPosition()).getId_page() - 1), Toast.LENGTH_LONG).show();
                    NetworkConnection.getInstance(getActivity()).execute(FragmentListaShots.this, String.valueOf(list_ListaItens.get(list_ListaItens.size() - 1).getId_page() - 1));
                }
            }
        });
        mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), mRecyclerView, this));

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

        AdapterShotRecycler adapter = new AdapterShotRecycler(getActivity(), list_ListaItens);
        mRecyclerView.setAdapter(adapter);

        activateSwipRefresh(layout, this, FragmentListaShots.class.getName());

        return layout;
    }

    public void activateSwipRefresh(final View view, final Transaction transaction, final String tag){
        // SWIPE REFRESH LAYOUT
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srl_swipe);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (Util.verifyConnection(getActivity())) {
                    if (list_ListaItens.get(0).getId_page() < 50){
                        NetworkConnection.getInstance(getActivity()).execute(transaction, String.valueOf(list_ListaItens.get(list_ListaItens.size()-1).getId_page()+1));
                        Toast.makeText(getActivity(), String.valueOf(list_ListaItens.get(0).getId_page() + 1), Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getActivity(), "Atualizado", Toast.LENGTH_LONG).show();
                        mSwipeRefreshLayout.setRefreshing(false);

                    }


                } else {
                    mSwipeRefreshLayout.setRefreshing(false);

                    android.support.design.widget.Snackbar.make(view, "Sem conexão com Internet. Por favor, verifique sey WiFi ou 3G.", android.support.design.widget.Snackbar.LENGTH_LONG)
                            .setAction("Ok", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //Intent it = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                    //startActivity(it);
                                }
                            })
                            .setActionTextColor(getActivity().getResources().getColor(R.color.coloLink))
                            .show();
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            list_ListaItens = savedInstanceState.getParcelableArrayList("list_ListaItens");
        }else {
            NetworkConnection.getInstance(getActivity()).execute(FragmentListaShots.this, "50");
        }
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("list_ListaItens", (ArrayList<Shot>) list_ListaItens);
    }



    //Network
    @Override
    public WrapObjToNetwork doBefore() {
        mPbLoad.setVisibility((mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) ? View.GONE : View.VISIBLE);

        if( Util.verifyConnection(getActivity()) ){
            Shot shot = new Shot();

            if( list_ListaItens != null && list_ListaItens.size() > 0 ){
                shot.setId(mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing() ? list_ListaItens.get(0).getId() : list_ListaItens.get(list_ListaItens.size() - 1).getId());
            }

            return( new WrapObjToNetwork(shot,  (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) ) );
        }
        return null;
    }

    @Override
    public void doAfter(JSONObject jSONObject) {
        mPbLoad.setVisibility(View.GONE );

        if( jSONObject != null ){
            AdapterShotRecycler adapter = (AdapterShotRecycler) mRecyclerView.getAdapter();
            Gson gson = new Gson();
            int auxPosition = 0, position;

            if( mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing() ){
                mSwipeRefreshLayout.setRefreshing(false);
                auxPosition = 1;
            }

            try{
                JSONArray jsonArray =  jSONObject.getJSONArray("shots");
                long page = jSONObject.getLong("page");
                for(int i = jsonArray.length()-1, tamI = 0; i >= tamI; i--){
                    Shot shot = gson.fromJson( jsonArray.getJSONObject(i).toString(), Shot.class );
                    shot.setId_page(page);
                    position = auxPosition == 0 ? list_ListaItens.size() : 0;
                    adapter.addListItem(shot, position);

                    if( auxPosition == 1 ){
                        mRecyclerView.getLayoutManager().smoothScrollToPosition(mRecyclerView, null, position);
                    }
                }

                if( jsonArray.length() == 0 && auxPosition == 0 ){
                    isLastItem = true;
                }

            }
            catch(JSONException e){
                Log.i(TAG, "doAfter(): " + e.getMessage());
            }
        }
        else{
            Toast.makeText(getActivity(), "Falhou. Tente novamente.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClickListener(View view, int position) {
        Intent intent = new Intent(getActivity(), ActivityShot.class);

        intent.putExtra("shot",  list_ListaItens.get(position));

        // TRANSITIONS
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){

            View ivShot = view.findViewById(R.id.iv_shot);
            View tv_titulo = view.findViewById(R.id.tv_titulo);
            View tv_views = view.findViewById(R.id.tv_views);

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    Pair.create(ivShot, "element1"),
                    Pair.create( tv_titulo, "element2" ),
                    Pair.create(tv_views, "element3" ));

            getActivity().startActivity( intent, options.toBundle() );
        }
        else{
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void onLongPressClickListener(View view, int position) {

    }

    public static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {
        private Context mContext;
        private GestureDetector mGestureDetector;
        private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerHack rvoclh){
            mContext = c;
            mRecyclerViewOnClickListenerHack = rvoclh;

            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if(cv != null && mRecyclerViewOnClickListenerHack != null){
                        mRecyclerViewOnClickListenerHack.onLongPressClickListener(cv,rv.getChildAdapterPosition(cv) );
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    boolean callContextMenuStatus = false;
                    if( cv instanceof CardView){
                        float x = ((RelativeLayout) ((CardView) cv).getChildAt(0)).getChildAt(2).getX();
                        float w = ((RelativeLayout) ((CardView) cv).getChildAt(0)).getChildAt(2).getWidth();
                        float y;// = ((RelativeLayout) ((CardView) cv).getChildAt(0)).getChildAt(3).getY();
                        float h = ((RelativeLayout) ((CardView) cv).getChildAt(0)).getChildAt(2).getHeight();

                        Rect rect = new Rect();
                        ((RelativeLayout) ((CardView) cv).getChildAt(0)).getChildAt(2).getGlobalVisibleRect(rect);
                        y = rect.top;

                        if( e.getX() >= x && e.getX() <= w + x && e.getRawY() >= y && e.getRawY() <= h + y ){
                            callContextMenuStatus = true;
                        }
                    }


                    if(cv != null && mRecyclerViewOnClickListenerHack != null && !callContextMenuStatus){
                        mRecyclerViewOnClickListenerHack.onClickListener(cv,rv.getChildAdapterPosition(cv) );
                    }

                    return(true);
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean b) {}
    }

}
