package ggc.desafioandroid;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import org.json.JSONObject;

import desafioandroid.modelos.Player;
import desafioandroid.modelos.Shot;
import desafioandroid.modelos.WrapObjToNetwork;
import ggc.desafioandroid.R;
import ggc.desafioandroid.network.NetworkConnection;
import ggc.desafioandroid.network.Transaction;
import me.drakeet.materialdialog.MaterialDialog;


/**
 * Created by Gilson on 19/09/2015.
 */
public class ActivityShot extends AppCompatActivity{
    private Toolbar mToolbar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private MaterialDialog mMaterialDialog;
    private Shot shot;
    private TextView tvDescription;
    private boolean isUsingTransition = false;
    private ViewGroup mRoot;
    private TextView tvLocation;
    private Button btAcao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TRANSITIONS
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){

            TransitionInflater inflater = TransitionInflater.from( this );
            Transition transition = inflater.inflateTransition( R.transition.transition );

            getWindow().setSharedElementEnterTransition(transition);

            Transition transition1 = getWindow().getSharedElementEnterTransition();
            transition1.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {
                    isUsingTransition = true;
                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    //TransitionManager.beginDelayedTransition(mRoot, new Slide());
                    tvDescription.setVisibility( View.VISIBLE );
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

        super.onCreate(savedInstanceState);
        Fresco.initialize(this);

        setContentView(R.layout.activity_shot);

        if(savedInstanceState != null){
            //Arrumar esse código para n ter que passar dois putExtras
            shot = savedInstanceState.getParcelable("shot");
        }
        else {
            if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getParcelable("shot") != null) {
                shot = getIntent().getExtras().getParcelable("shot");
            }
            else {
                Toast.makeText(this, "Fail!", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        //ToolBar
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCollapsingToolbarLayout.setTitle(shot.getTitle());

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle(shot.getTitle());
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tvLocation = (TextView) findViewById(R.id.tv_location);

        mRoot = (ViewGroup) findViewById(R.id.ll_tv_description);
        tvDescription = (TextView) findViewById(R.id.tv_description);
        SimpleDraweeView ivShot = (SimpleDraweeView) findViewById(R.id.iv_shot);
        SimpleDraweeView ivAvatar_url = (SimpleDraweeView) findViewById(R.id.iv_avatar_url);
        TextView tvNome = (TextView) findViewById(R.id.tv_nome);
        TextView tvWebSite = (TextView) findViewById(R.id.tv_WebSite);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize( size );
        int w;
        try{
            w = size.x;
        }
        catch( Exception e ){
            w = display.getWidth();
        }

        Uri uri = Uri.parse(shot.getImage_url());
        DraweeController dc = Fresco.newDraweeControllerBuilder()
                .setUri( uri )
                .setAutoPlayAnimations(true)
                .setOldController( ivShot.getController() )
                .build();
        ivShot.setController(dc);


        Uri uri2  = Uri.parse(shot.getPlayer().getAvatar_url());
        DraweeController dc2 = Fresco.newDraweeControllerBuilder()
                .setUri( uri2 )
                .setAutoPlayAnimations(true)
                .setOldController( ivAvatar_url.getController() )
                .build();

        ivAvatar_url.setController(dc2);

        tvNome.setText(Html.fromHtml(shot.getPlayer().getName() != null ? shot.getPlayer().getName() : ""));
        tvWebSite.setText(shot.getPlayer().getWebsite_url() != null?shot.getPlayer().getWebsite_url():"");
        tvLocation.setText(Html.fromHtml(shot.getPlayer().getLocation() == null ? "" : shot.getPlayer().getLocation()));
        tvDescription.setText(Html.fromHtml(shot.getDescription()!=null?shot.getDescription():""));
        tvDescription.setVisibility(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP || savedInstanceState != null || !isUsingTransition ? View.VISIBLE : View.INVISIBLE);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, shot.getUrl());
                startActivity(Intent.createChooser(intent, "Desafio android"));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
