package ggc.desafioandroid;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;


public class FragmentListaShots extends Fragment {

    private TextView mConteudo;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_lista_shots, container, false);

        mConteudo = (TextView) layout.findViewById(R.id.tv_Conteudo);

        CustomJsonRequest request = new CustomJsonRequest
                (Request.Method.GET, ActivityPrincipal.RECENT_API_ENDPOINT, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String minTemp, maxTemp, atmo;
                            int avgTemp;

                            response = response.getJSONObject("report");

                            //minTemp = response.getString("min_temp"); minTemp = minTemp.substring(0, minTemp.indexOf("."));
                            //maxTemp = response.getString("max_temp"); maxTemp = maxTemp.substring(0, maxTemp.indexOf("."));

                            //avgTemp = (Integer.parseInt(minTemp)+Integer.parseInt(maxTemp))/2;



                        } catch (Exception e) {
                            //txtError(e);
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //txtError(error);
                    }
                });

        request.setPriority(Request.Priority.HIGH);
        ActivityPrincipal.helper.add(request);

        return layout;
    }
}
