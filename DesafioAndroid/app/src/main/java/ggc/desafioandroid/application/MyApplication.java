package ggc.desafioandroid.application;

import android.app.Application;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Gilson on 17/09/2015.
 */
public class MyApplication extends Application {

    private RequestQueue mRequestQueue;
    private static MyApplication mApplication;
    private static Context mContext;
    public static final String TAG = MyApplication.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    public static synchronized MyApplication getInstance() {
        return mApplication;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public <T> void add(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancel() {
        mRequestQueue.cancelAll(TAG);
    }
}
