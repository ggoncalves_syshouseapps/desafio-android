package ggc.desafioandroid.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Extras.Util;
import desafioandroid.modelos.ContextMenuItem;
import desafioandroid.modelos.Shot;
import ggc.desafioandroid.R;
import interfaces.RecyclerViewOnClickListenerHack;

/**
 * Created by Gilson on 17/09/2015.
 */
public class AdapterShotRecycler extends RecyclerView.Adapter<AdapterShotRecycler.MyViewHolder> {
    private Context mContext;
    private List<Shot> mList;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private float scale;
    private int width, height, roundPixels;

    private boolean withAnimation;

    public AdapterShotRecycler(Context c, List<Shot> l){
        this(c, l, true);
    }
    public AdapterShotRecycler(Context c, List<Shot> l, boolean wa){
        mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        withAnimation = wa;

        scale = mContext.getResources().getDisplayMetrics().density;
        width = mContext.getResources().getDisplayMetrics().widthPixels - (int)(14 * scale + 0.5f);
        height = (width / 16) * 9;

        roundPixels = (int)(2 * scale + 0.5f);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = mLayoutInflater.inflate(R.layout.item_shot, viewGroup, false);

        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int position) {

        myViewHolder.llViews.setVisibility(View.GONE);
        myViewHolder.llHearts.setVisibility(View.GONE);
        myViewHolder.llComments.setVisibility(View.GONE);

        if ( mList.get(position).getViews_count() > 0){
            myViewHolder.llViews.setVisibility(View.VISIBLE);
            myViewHolder.tvViews.setText(String.valueOf(mList.get(position).getViews_count()));
        }
        if ( mList.get(position).getLikes_count() > 0){
            myViewHolder.llHearts.setVisibility(View.VISIBLE);
            myViewHolder.tvHearts.setText(String.valueOf(mList.get(position).getLikes_count()));
        }
        if ( mList.get(position).getComments_count() > 0){
            myViewHolder.llComments.setVisibility(View.VISIBLE);
            myViewHolder.tvComments.setText(String.valueOf(mList.get(position).getComments_count()));
        }

        myViewHolder.tvTitulo.setText(Html.fromHtml(mList.get(position).getTitle() == null ? "" : mList.get(position).getTitle()));


        ControllerListener listener = new BaseControllerListener(){
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                //Log.i("LOG", "onFinalImageSet");
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                //Log.i("LOG", "onFailure");
            }

            @Override
            public void onIntermediateImageFailed(String id, Throwable throwable) {
                super.onIntermediateImageFailed(id, throwable);
                //Log.i("LOG", "onIntermediateImageFailed");
            }

            @Override
            public void onIntermediateImageSet(String id, Object imageInfo) {
                super.onIntermediateImageSet(id, imageInfo);
                //Log.i("LOG", "onIntermediateImageSet");
            }

            @Override
            public void onRelease(String id) {
                super.onRelease(id);
                //Log.i("LOG", "onRelease");
            }

            @Override
            public void onSubmit(String id, Object callerContext) {
                super.onSubmit(id, callerContext);
                //Log.i("LOG", "onSubmit");
            }
        };

        Uri uri = Uri.parse( mList.get(position).getImage_teaser_url());
        DraweeController dc = Fresco.newDraweeControllerBuilder()
                .setUri( uri )
                .setTapToRetryEnabled(true)
                .setAutoPlayAnimations(true)
                .setControllerListener( listener )
                .setOldController( myViewHolder.ivShot.getController() )
                .build();

        RoundingParams rp = RoundingParams.fromCornersRadii(roundPixels, roundPixels, 0, 0);
        myViewHolder.ivShot.setController(dc);
        myViewHolder.ivShot.getHierarchy().setRoundingParams( rp );


        if(withAnimation){
            try{
                YoYo.with(Techniques.Tada).duration(800).playOn(myViewHolder.itemView);
            }
            catch(Exception e){}
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void addListItem(Shot c, int position){
        mList.add(position, c);
        notifyItemInserted(position);
    }


    public void removeListItem(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public SimpleDraweeView ivShot;
        public TextView tvTitulo;
        public TextView tvViews;
        public TextView tvHearts;
        public TextView tvComments;
        public LinearLayout llViews;
        public LinearLayout llHearts;
        public LinearLayout llComments;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivShot = (SimpleDraweeView) itemView.findViewById(R.id.iv_shot);
            tvTitulo = (TextView) itemView.findViewById(R.id.tv_titulo);
            tvViews = (TextView) itemView.findViewById(R.id.tv_views);
            tvHearts = (TextView) itemView.findViewById(R.id.tv_heart);
            tvComments = (TextView) itemView.findViewById(R.id.tv_comments);
            llViews = (LinearLayout) itemView.findViewById(R.id.ll_views);
            llHearts = (LinearLayout) itemView.findViewById(R.id.ll_heart);
            llComments = (LinearLayout) itemView.findViewById(R.id.ll_comments);

        }
    }

}


