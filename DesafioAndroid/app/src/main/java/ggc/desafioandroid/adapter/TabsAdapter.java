package ggc.desafioandroid.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ggc.desafioandroid.R;
import ggc.desafioandroid.fragments.FragmentListaShots;
import ggc.desafioandroid.fragments.FragmentPerfil;

/**
 * Created by Gilson on 19/09/2015.
 */
public class TabsAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] titles = {"SHOTS", "PERFIL"};
    private int[] icons = new int[]{R.drawable.ic_plus,R.drawable.ic_open};
    private int heightIcon;


    public TabsAdapter(FragmentManager fm, Context c) {
        super(fm);

        mContext = c;
        double scale = c.getResources().getDisplayMetrics().density;
        heightIcon = (int)( 24 * scale + 0.5f );
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;

        if(position == 0){ // Shots
            frag = new FragmentListaShots();
        }
        else if(position == 1){ // Perfil
            frag = new FragmentPerfil();
        }
        Bundle b = new Bundle();
        b.putInt("position", position);

        frag.setArguments(b);

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return ( titles[position] );
    }
}
