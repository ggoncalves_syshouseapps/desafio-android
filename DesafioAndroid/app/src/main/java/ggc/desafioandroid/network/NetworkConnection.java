package ggc.desafioandroid.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import desafioandroid.modelos.Player;
import desafioandroid.modelos.Shot;
import desafioandroid.modelos.WrapObjToNetwork;

/**
 * Created by Gilson on 18/09/2015.
 */
public class NetworkConnection {
    private static NetworkConnection instance;
    private Context mContext;
    private RequestQueue mRequestQueue;


    public NetworkConnection(Context c){
        mContext = c;
        mRequestQueue = getRequestQueue();
    }


    public static NetworkConnection getInstance( Context c ){
        if( instance == null ){
            instance = new NetworkConnection( c.getApplicationContext() );
        }
        return( instance );
    }


    public RequestQueue getRequestQueue(){
        if( mRequestQueue == null ){
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return(mRequestQueue);
    }


    public <T> void addRequestQueue( Request<T> request ){
        getRequestQueue().add(request);
    }


    public void execute( final Transaction transaction, final String tag ){
        WrapObjToNetwork obj = transaction.doBefore();
        Gson gson = new Gson();

        if( obj == null ){
            return;
        }

        CustomJsonRequest request = new CustomJsonRequest(Request.Method.GET,"http://api.dribbble.com/shots/popular?page=" + tag ,obj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        transaction.doAfter(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("LOG", "onErrorResponse(): " + error.getMessage());
                        transaction.doAfter(null);
                    }
                });
        addRequestQueue(request);
    }
}
