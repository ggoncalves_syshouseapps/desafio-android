package ggc.desafioandroid.network;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Gilson on 17/09/2015.
 */
public class CustomJsonRequest extends JsonObjectRequest {

    private Response.Listener<JSONArray> listener;
    public CustomJsonRequest(int method, String url, JSONObject jsonRequest,Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    private Priority mPriority;

    public void setPriority(Priority priority) {
        mPriority = priority;
    }

    @Override
    public Priority getPriority() {
        return mPriority == null ? Priority.NORMAL : mPriority;
    }

}
