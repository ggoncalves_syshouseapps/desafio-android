package ggc.desafioandroid.network;

import org.json.JSONArray;
import org.json.JSONObject;

import desafioandroid.modelos.Player;
import desafioandroid.modelos.WrapObjToNetwork;

/**
 * Created by Gilson on 18/09/2015.
 */
public interface Transaction {
    WrapObjToNetwork doBefore();

    void doAfter(JSONObject jsonArray);
}
